﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Penjualan_App.Services;
using Penjualan_App.Views;

namespace Penjualan_App
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            DependencyService.Register<MockDataStore>();
            MainPage = new AppShell();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
