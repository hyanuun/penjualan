﻿namespace Penjualan_App.Win10.Models
{
    public class Barang
    {
        public int id_barang{ get; set; }
        public string nama { get; set; }
        
        public double harga { get; set; }
        public int stok { get; set; }
        public string deskripsi { get; set; }

        //foreign key
        //public virtual JenisBarang JenisBarang { get; set; }
        //public int id_jenis { get; set; }
        //public virtual JenisBarang JenisBarang { get; set; }

    }
}
