﻿namespace Penjualan_App.Win10.Models
{
    public class Pemasok
    {
        public int id { get; set; }
        public int id_barang { get; set; }
        public int jumlah { get; set; }
        public string nama_distributor { get; set; }
        public string nm_perusahaan { get; set; }
        public string telp { get; set; }
        public string alamat { get; set; }
    }
}
