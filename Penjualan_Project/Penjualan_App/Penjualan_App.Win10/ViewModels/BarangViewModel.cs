﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Text;
using System.Windows.Input;
using Penjualan_App.Win10.Models;

namespace Penjualan_App.Win10.ViewModels
{
    public class BarangViewModel : BaseViewModel
    {
        public BarangViewModel()
        {
            databarang = new ObservableCollection<Barang>();
            modelbarang = new Barang();

            CreateCommand = new Command(async () => await CreateBarangAsync());
            UpdateCommand = new Command(async () => await UpdateBarangAsync());
            DeleteCommand = new Command(async () => await DeleteBarangAsync());
            ReadCommand = new Command(async () => await ReadBarangAsync(true));
            ReadCommand.Execute(null);

        }

        public ICommand CreateCommand { get; set; }
        public ICommand ReadCommand { get; set; }
        public ICommand UpdateCommand { get; set; }
        public ICommand DeleteCommand { get; set; }

        public ObservableCollection<Barang> DataBarang
        {
            get
            {
                return databarang;
            }

            set
            {
                SetProperty(ref databarang, value);
            }
        }

        public Barang ModelBarang
        {
            get
            {
                return modelbarang;
            }

            set
            {
                SetProperty(ref modelbarang, value);
            }
        }

        public event Action OnReload;

        private ObservableCollection<Barang> databarang;
        private Barang modelbarang;

        private async Task InitBarangAsync()
        {
            await Task.Run(() => {
                DataBarang = new ObservableCollection<Barang>
                {
                    new Barang {id_barang=1, nama="Kemeja", harga = 20000, stok=54, deskripsi="Kemeja Flanel Hijau motif kotak-kotak dengan bahan lembut"},
                    new Barang {id_barang=2, nama="Rok", harga = 100000, stok=2, deskripsi="Rok Merah berbahan satin"},
                    new Barang {id_barang=3, nama="Sepatu", harga = 200000, stok=4, deskripsi="Sepatu dengan model terbaru ini sangat highclass"},
                    new Barang {id_barang=4, nama="Gamis", harga = 500000, stok=5, deskripsi="Gamis dengan warna pastel yang sangat memanjakan mata dan enak dipandang"},
                    new Barang {id_barang=5, nama="Blouse", harga = 430000, stok=26, deskripsi="Blouse bestseller dengan bahan yang menyerap keringat dan tidak transparan"},
                };
            });
        }

        private async Task ReadBarangAsync(bool asnew = false)
        {
            if (asnew)
            {
               await InitBarangAsync();
            }
            else
            {
                OnReload?.Invoke();
            }
        }

        private async Task<Barang> ReadBarangAsync(int id_barang)
        {
            await Task.Delay(0);
            return DataBarang.Where(model => model.id_barang.Equals(id_barang)).SingleOrDefault();
        }

        private async Task CreateBarangAsync()
        {
            DataBarang.Add(ModelBarang);
            await ReadBarangAsync();
        }

        private async Task UpdateBarangAsync()
        {
            var data = ModelBarang;
            DataBarang.Remove(ReadBarangAsync(data.id_barang).Result);
            DataBarang.Add(ModelBarang);
            await ReadBarangAsync();
        }

        private async Task DeleteBarangAsync()
        {
            DataBarang.Remove(ModelBarang);
            await ReadBarangAsync();
        }
    }
}
