﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Penjualan_App.Win10.Views.Home
{
    /// <summary>
    /// Interaction logic for Dashboard.xaml
    /// </summary>
    public partial class Dashboard : Window
    {
        public Dashboard()
        {
            InitializeComponent();
        }

        private void ButtonProfile_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ButtonCustomer_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ButtonCustomer_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void ButtonKaryawan_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ButtonPemasok_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ButtonTransaksi_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ButtonBarang_Click(object sender, RoutedEventArgs e)
        {
            new MasterData.BarangForm().ShowDialog();
        }

        private void MenuProfile_Click(object sender, RoutedEventArgs e)
        {

        }

        private void MenuExit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void MenuBarang_Click(object sender, RoutedEventArgs e)
        {

        }

        private void MenuPemasok_Click(object sender, RoutedEventArgs e)
        {

        }

        private void MenuCustomer_Click(object sender, RoutedEventArgs e)
        {

        }

        private void MenuKaryawan_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
